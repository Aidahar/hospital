class CreateAppointments < ActiveRecord::Migration[5.2]
  def change
    create_table :appointments do |t|
      t.references :physician, foreign_key: true
      t.date :date
      t.time :time
      t.string :patient

      t.timestamps
    end
  end
end
