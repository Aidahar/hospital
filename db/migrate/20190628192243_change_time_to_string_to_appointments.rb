class ChangeTimeToStringToAppointments < ActiveRecord::Migration[5.2]
  def up
    change_column :appointments, :time, :string
  end

  def down
    change_column :appointments, :time, :time
  end
end
