class PhysiciansController < ApplicationController
  def new
    @physician = Physician.new
  end

  def create
    @physician = Physician.new(physician_params)
    
    if @physician.save
      redirect_to root_path "Карточка врача создана."
    else
      render 'new'
    end
  end

  def edit
    @physician = Physician.find(params[:id])
  end

  def update
    @physician = Physician.find(params[:id])
    if @physician.update(physician_params)
      redirect_to physicians_path, notice: "Карточка обновлена"
    else
      render 'edit'
    end
  end

  def index
    @physicians = Physician.all
  end

  def show
    @physician = Physician.find(params[:id]) 
  end

  private
  
  def physician_params
    params.require(:physician).permit(:first_name, :last_name, :specialty)
  end
end
