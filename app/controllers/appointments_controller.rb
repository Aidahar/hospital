class AppointmentsController < ApplicationController
  def index
    @appointments = Appointment.all
    @physicians = Physician.all
  end

  def new
    @appointment = Appointment.new
  end

  def create
    @appointment = Appointment.new(appointment_params)

    if @appointment.save
      redirect_to root_path, notice: "Запись создана"
    else
      render 'new'
    end
  end

  def edit
    @appointment = Appointment.find(params[:id])
  end

  def update
    @appointment = Appointment.find(params[:id])
    if @appointment.update(appointment_params)
      redirect_to root_url, notice: "Карточка обновлена"
    else
      render 'edit'
    end
  end

  def show
    @appointment = Appointment.find(params[:id])
  end

  private

  def appointment_params
    params.require(:appointment).permit(:physician_id, :date, :time, :patient)
  end
end
