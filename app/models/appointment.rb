class Appointment < ApplicationRecord
  belongs_to :physician
  validates :patient, :physician_id, :date, :time, presence: true
  validates :time, uniqueness: { scope: :physician }
end
