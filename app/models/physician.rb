class Physician < ApplicationRecord
  has_many :appointments
  validates :first_name, :specialty, :last_name, presence: true
end
